﻿using System;
using System.Collections.Generic;

namespace SudokuTest
{
    class Program
    {

        static void Main(string[] args)
        {
            
            List<string> exampleNames = GetExampleNames();

            System.Console.WriteLine("Hello");
            System.Console.WriteLine("Please choose one of the number below " +
                                     "to test");
            System.Console.WriteLine("Available Names are :");

            for (int i = 0; i < exampleNames.Count; i++){
                System.Console.WriteLine("{0}.{1}",i + 1, exampleNames[i]);
            }
           
           
            int choice = int.Parse(System.Console.ReadLine());

            InitGame(choice);
           

        }

        public static class GlobalValues
        {
            public static int rows = 0;
            public static int columns = 0;
            public static int outsideSquare = 0;
            public static int insideSquare = 0;
        }

        static void InitGame(int choice){

            int[][] goodSudoku1 = {
                new int[] {7,8,4,  1,5,9,  3,2,6},
                new int[] {5,3,9,  6,7,2,  8,4,1},
                new int[] {6,1,2,  4,3,8,  7,5,9},

                new int[] {9,2,8,  7,1,5,  4,6,3},
                new int[] {3,5,7,  8,4,6,  1,9,2},
                new int[] {4,6,1,  9,2,3,  5,8,7},

                new int[] {8,7,6,  3,9,4,  2,1,5},
                new int[] {2,4,3,  5,6,1,  9,7,8},
                new int[] {1,9,5,  2,8,7,  6,3,4}
            };


            int[][] goodSudoku2 = {
                new int[] {1,4, 2,3},
                new int[] {3,2, 4,1},

                new int[] {4,1, 3,2},
                new int[] {2,3, 1,4}
            };


            int[][] badSudoku1 =  {
                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},

                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},

                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9},
                new int[] {1,2,3, 4,5,6, 7,8,9}
            };

            int[][] badSudoku2 = {
                new int[] {1,2,3,4,5},
                new int[] {1,2,3,4},
                new int[] {1,2,3,4},
                new int[] {1}
            };

           

            if (choice > GetExampleNames().Count)
                System.Console.WriteLine("Not valid choice. Bye");

            else
            {
                if (checkIfGameContraintsAreCorrect(
                    choice == 1 ? goodSudoku1 : choice == 2 ? goodSudoku2 :
                    choice == 3 ? badSudoku1 : badSudoku2))
                {
                    if (checkIfGameValuesAreCorrect(
                        choice == 1 ? goodSudoku1 : choice == 2 ? goodSudoku2 :
                         choice == 3 ? badSudoku1 : badSudoku2))
                        System.Console.Write("Sudoku is correct");
                    else
                        System.Console.Write("Sudoku values are not correct");
                }
                else
                    System.Console.Write("Sudoku contrainst are  not correct");
            }

        }

        static List<string> GetExampleNames()
        {
            List<string> sudokuNames = new List<string>();
            sudokuNames.Add("Good Sudoku 1");
            sudokuNames.Add("Good Sudoku 2");
            sudokuNames.Add("Bad Sudoku 1");
            sudokuNames.Add("Bad Sudoku 2");
            return sudokuNames;
        }


        // This method is responsible to check if all contrains of the sudoku
        // are correct. For example if sudoku rows are different from columns
        // then is not correct.
        private static bool checkIfGameContraintsAreCorrect(int[][] array)
        {
            if (array == null || array.Length == 0)
                return false;

            GlobalValues.rows = array.Length;
            GlobalValues.columns = array[0].Length;
            GlobalValues.outsideSquare = array.Length;
            GlobalValues.insideSquare = calculateSquareRoot(GlobalValues.rows);

            if (GlobalValues.rows != GlobalValues.columns ||
                GlobalValues.insideSquare == -1)

                return false;
            else
                return true;

        }


        // This method is responsible to find the length of the inside 
        // squares of the sudoku. For example if rows are nine then the root is 3
        // so we have inside sqaures of 3
        private static int calculateSquareRoot(int rows)
        {
            if (rows < 0)
                return -1;

            switch (rows)
            {
                case 0:
                case 1:
                case 4:
                case 9:
                    int root = (int)Math.Sqrt(rows);

                    if (root * root == rows)
                        return root;
                    else
                        return -1;

                default:
                    return -1;
            }
        }

        // This method is responsible to check all the values of Sudoku
        private static bool checkIfGameValuesAreCorrect(int[][] sudoku)
        {

            /// Check in rows if there are any recurrencies
            for (int i = 0; i < sudoku.Length; i++)
                if (!checkValuesIfAreCorrect(sudoku[i]))
                    return false;

            /// Check in columns if there are any recurrencies
            for (int k = 0; k < sudoku.Length; k++)
            {
                int[] sudokuColumn = new int[sudoku.Length];

                for (int i = 0; i < sudoku.Length; i++)
                    sudokuColumn[i] = sudoku[i][k];

                //Check if values arre correct
                if (!checkValuesIfAreCorrect(sudokuColumn))
                    return false;
            }

            return true;

        }

        private static bool checkValuesIfAreCorrect(int[] rowValues)
        {

            //rowValues = source array
            //0 = start index in source array
            //temp = destination array
            //0 = start index in destination array
            //rowValues.Length = elements to copy

            int[] temp = new int[rowValues.Length];
            Array.Copy(rowValues, 0, temp, 0, rowValues.Length);
            Array.Sort(temp);

            for (int i = 0; i < rowValues.Length; i++)
            {
                // System.Console.Write("Number({0}): ", temp[i]);
              
                if (temp[i] > 0)
                {
                    if (temp[i] != i + 1)
                        return false;
                }
                else
                    return false;
              
            }
            return true;
        }


    }

}
